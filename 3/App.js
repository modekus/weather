import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View , 
  Platform,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground
} from 'react-native';

import SearchInput from './components/SearchInput';

import getImageForWeather from '../utils/getImageForWeather';


export default class App extends React.Component {
  render() {
    
    return (
      <KeyboardAvoidingView style={styles.container} behavior="position">
      <ImageBackground
                source={getImageForWeather('Clear')}
                style={styles.imageContainer}
                imageStyle={styles.image}>
                
                 <Text style={[styles.largeText, styles.textStyle]}>San Francisco</Text>
                 <Text style={[styles.smallText, styles.textStyle]}>Light Cloud</Text>
                 <Text style={[styles.largeText, styles.textStyle]}>24°</Text>

                 <SearchInput placeholder="Search any city"/>
      
      
      
      </ImageBackground>

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccceee',
    alignItems: 'center',
    justifyContent: 'center',
  },
  red: {
    color: 'red',
    },
  textStyle: {
    textAlign : 'center',
    ...Platform.select({
      ios:{
        fontFamily : 'AvenirNext-Regular',
      },
      android: {
        fontFamily : 'Roboto',
      }
    })
  },
  largeText : {
    fontSize : 44
  },
  smallText : {
    fontSize: 18
  },
  
});
