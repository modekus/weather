import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground,
  ActivityIndicator,
  StatusBar,
  Dimensions,
  SafeAreaView,
  Easing
} from 'react-native';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

import { fetchLocationId, fetchWeather, fetchLocationIdByLatLng, fetchPhotosFromUSplash } from '../utils/api';
import getImageForWeather from '../utils/getImageForWeather';

import SearchInput from './components/SearchInput';
import GoogleSearchInput from './components/GoogleSearchInput';

  const windowSize = require('Dimensions').get('window')
    const deviceWidth = windowSize.width;
    const deviceHeight = windowSize.height;


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      loading: false,
      error: false,
      temperature: 72,
      weather: '',
      photo: 'https://hdqwalls.com/download/san-francisco-bridge-aerial-view-5k-bw-1440x2960.jpg'
    }
  }

  componentWillMount() {
    this.handleUpdateLocation('San Francisco');
  }

  handleUpdateLocation = async (city, lat, lng, photo = '') => {

    if (!city) return;


    this.setState({ loading: true }, async () => {
      try {
        const locationId = await ((!lat) ? fetchLocationId(city) : fetchLocationIdByLatLng(lat, lng));

        const { weather, temperature } = await fetchWeather(locationId);

        await fetchPhotosFromUSplash(city)
          .then(res => res.json())
          .then(data => {
            var results = data['results'];
            var photoRow = results[Math.floor(Math.random() * results.length)];
            var photo = photoRow['urls']['raw'];
            this.setState({ photo: photo });
          })
          .catch(err => {
            return err;
          });


        const location = city;



        this.setState({
          loading: true,
          error: false,
          location,
          weather,
          temperature,

        });
      }
      catch (e) {
        console.log(e);
        this.setState({
          loading: false,
          error: true,
        });
      }
    });
  }




  render() {
    const { loading, error, location, weather, temperature, photo } = this.state;
  

    return (
      
        <KeyboardAvoidingView style={styles.container} behavior="padding">
          <StatusBar barStyle="light-content" hidden={true} />
          <ImageBackground
            //source={getImageForWeather(weather)}

            //source={loading ? getImageForWeather(weather) : {uri:photo}}
            source={{ uri: photo }}
            onLoad={() => { this.setState({ loading: false }) }}
            onError={() => { throws.setState({ photo: getImageForWeather(weather) }) }}
            style={styles.imageContainer}
            imageStyle={styles.image}>

            <View style={styles.detailsContainer}>
              <View>
                {/* <ActivityIndicator animating={loading} color="white" size="large" style={{ top: (deviceHeight / 2)-40, right: deviceWidth / 2-40, position: 'absolute' }}></ActivityIndicator> */}
                {loading &&
                <BarIndicator animating={loading}  size={60} count = {6} color = 'white' style={{ top: (deviceHeight / 2)-40, right: deviceWidth / 2-90, position: 'absolute' }}/>
                }
              </View>
              {!loading && (
                <View>
                  {error && (
                    <Text style={[styles.smallText, styles.textStyle]}>
                      Could not load weather, please try a different city
                      </Text>
                  )}

                  {!error && (

                    <View>
                      <Text style={[styles.largeText, styles.textStyle]}>{location}</Text>
                      <Text style={[styles.smallText, styles.textStyle]}>{weather}</Text>
                      <Text style={[styles.largeText, styles.textStyle]}>{`${Math.round(temperature)}°`}F</Text>
                    </View>
                  )}


                  {/* <SearchInput placeholder="Search any city" onSubmit={this.handleUpdateLocation}/> */}
                  {<GoogleSearchInput onSubmit={this.handleUpdateLocation} />}

                </View>

              )}


            </View>


          </ImageBackground>

        </KeyboardAvoidingView>
     
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#34495E',
    

  },
  detailsContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 20,
    paddingVertical:40

  },
  imageContainer: {
    flex: 1
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  },
  textStyle: {
    textAlign: 'center',
    color: '#ecf0f1',
    ...Platform.select({
      ios: {
        fontFamily: 'AvenirNext-Regular',
      },
      android: {
        fontFamily: 'AvenirNext-Regular',
      }
    })
  },
  largeText: {
    ...Platform.select({
      ios: {
        fontSize: 30,
      },
      android: {
        fontSize: 28,
      }
    })
  },
  smallText: {
    fontSize: 18
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }

});
