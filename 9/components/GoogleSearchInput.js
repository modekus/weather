import React from "react";
import { View, Image, StyleSheet, Platform, Dimensions } from "react-native";
import {
  GooglePlacesAutocomplete
} from "react-native-google-places-autocomplete";

const API_KEY='AIzaSyCiZg3s4lUij2o9rZm9NRi4r5EOaTJllhg';

export default class GoogleSearchInput extends React.Component {
  constructor(props) {
    super(props);
   
    this.state = {
      location: 'San Francisco',
      lng:'',
      lat:'',
      photo:'',
    };
  }

 

  handleChangeText = (latlng, location, photos) => {
    var photoUrl = null;
    if(photos!=null){
      var photo = photos[Math.floor(Math.random()*photos.length)];
      var urlParent = photo['html_attributions'];
    
      var url = urlParent[0];
      var href = url.match(/href="([^"]*)/)[1];
      var photoReference = photo['photo_reference'];
      photoUrl = "https://maps.googleapis.com/maps/api/place/photo?photoreference="+photoReference+"&key="+API_KEY+"&maxheight=1242&maxwidth=2208";
    }
      this.setState({ location: location, lat: latlng['lat'], lng: latlng['lng'], photo: photoUrl});
    this.handleSubmit()
  };

  handleSubmit = () => {
    //this.setState()
    const { onSubmit } = this.props;
    const { location, lng, lat, photo } = this.state;
    

    if (!location) return;

    onSubmit(location, lat, lng, photo);
    
  };
  
  render() {
    const windowSize = require('Dimensions').get('window')
    const deviceWidth = windowSize.width-20;
    const deviceHeight = windowSize.height;
    const { onSubmit, onFocus } = this.props;
    onFocus(true);
    return (
        <View style={styles.container} >
            <GooglePlacesAutocomplete
            placeholder="Enter City or Zip/Pin Code"
            minLength={2}
            autoFocus={false}
            returnKeyType={"default"}
            fetchDetails={true}
            renderDescription={row => row.description}
         
            GooglePlacesSearchQuery={{
                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                rankby: 'distance',
                types: 'regions'
            }}
            debounce={0}
            query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key: API_KEY,
                language: 'en', // language of the results
                types: '(regions)' // default: 'geocode'
              }}
            styles={{
                textInputContainer: {
                backgroundColor: "rgba(0,0,0,0)",
                borderTopWidth: 0,
                borderBottomWidth: 0
                },
                textInput: {
                marginLeft: 0,
                marginRight: 0,
                height: 38,
                color: "#5d5d5d",
                fontSize: 16
                },
                predefinedPlacesDescription: {
                color: "#1faadb"
                },
                listView: {  
                  backgroundColor: '#34495e',          
                  width: 280,
                  position: 'absolute',
                  marginTop: 40,
                  elevation: 1
                  
                },
               
                description: {
                  color: 'white',
                  ...Platform.select({
                    ios:{
                      fontSize : 16,
                    },
                    android: {
                      fontSize : 14,
                    }
                  })
                },
                listViewDisplayed: 'true'

            }}
            getDefaultValue
            ={() => ''}
            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
            currentLocationLabel="Current location"
            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
              this.handleChangeText(details['geometry']['location'], data['description'],details['photos']);
            }}
            listViewDisplayed= 'true'
            onSubmit={(data, details = null) => { // 'details' is provided when fetchDetails = true
               this.handleChangeText(details['geometry']['location'], data['description'],details['photos']);
            }}

            />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //backgroundColor: "#ecf0f1",
    flex: 1,
    height: 30,
    width: 300,
    marginTop: 20,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    alignSelf: "center",
  },
  textInput: {
    flex: 1,
    color: "#2c3e50",
    fontSize: 16
  }
});
