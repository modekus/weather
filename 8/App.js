import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground,
  ActivityIndicator,
  StatusBar,
  Dimensions,
  SafeAreaView,
  Easing
} from 'react-native';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';
import GridView from 'react-native-super-grid';
import DailyWeather from './components/DailyWeather';

import { fetchLocationId, fetchWeather, fetchLocationIdByLatLng, fetchPhotosFromUSplash } from '../utils/api';
import getImageForWeather from '../utils/getImageForWeather';
import { getCelciusFromF, getFahreinheitFromC } from '../utils/tempConversions';

import SearchInput from './components/SearchInput';
import GoogleSearchInput from './components/GoogleSearchInput';
import { ScrollView } from 'react-native-gesture-handler';

const windowSize = require('Dimensions').get('window')
const deviceWidth = windowSize.width;
const deviceHeight = windowSize.height;


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      loading: false,
      error: false,
      temperature: 22.22,
      weather: '',
      photo: 'https://hdqwalls.com/download/san-francisco-bridge-aerial-view-5k-bw-1440x2960.jpg',
      consolidatedWeather: [],
      placeSearch: false
    }
  }

  componentWillMount() {
    this.handleUpdateLocation('San Francisco');
  }

  handleSearchFocus = async (searchstatus) => {
    //this.setState({placeSearch:searchstatus});
    //console.log("Calling Seach Status");
  }

  handleUpdateLocation = async (city, lat, lng, photo = '') => {

    if (!city) return;


    this.setState({ loading: true }, async () => {
      try {
        const locationId = await ((!lat) ? fetchLocationId(city) : fetchLocationIdByLatLng(lat, lng));

        const { weather, temperature, consolidatedWeather } = await fetchWeather(locationId);


        await fetchPhotosFromUSplash(city)
          .then(res => res.json())
          .then(data => {
            var results = data['results'];
            var photoRow = results[Math.floor(Math.random() * results.length)];
            var photo = photoRow['urls']['raw'];
            this.setState({ photo: photo });
          })
          .catch(err => {
            return err;
          });


        const location = city;



        this.setState({
          loading: true,
          error: false,
          location,
          weather,
          temperature,
          consolidatedWeather,
          placeSearch: false

        });
      }
      catch (e) {
        console.log(e);
        this.setState({
          loading: false,
          error: true,
        });
      }
    });
  }




  render() {
    const { loading, error, location, weather, temperature, photo, consolidatedWeather, placeSearch } = this.state;


    return (

      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <StatusBar barStyle="light-content" hidden={true} />
        <ImageBackground
          //source={getImageForWeather(weather)}

          //source={loading ? getImageForWeather(weather) : {uri:photo}}
          source={{ uri: photo }}
          onLoad={() => { this.setState({ loading: false }) }}
          onError={() => { throws.setState({ photo: getImageForWeather(weather) }) }}
          style={styles.imageContainer}
          imageStyle={styles.image}>
          <ScrollView keyboardShouldPersistTaps="always">
          <View style={styles.detailsContainer}>

            <View>
              {/* <ActivityIndicator animating={loading} color="white" size="large" style={{ top: (deviceHeight / 2)-40, right: deviceWidth / 2-40, position: 'absolute' }}></ActivityIndicator> */}
              {loading &&
                <BarIndicator animating={loading} size={60} count={6} color='white' style={{ top: (deviceHeight / 2) - 40, right: deviceWidth / 2 - 90, position: 'absolute' }} />
              }
            </View>
            {!loading && (
              <View>
                {error && (
                  <Text style={[styles.smallText, styles.textStyle]}>
                    Could not load weather, please try a different city
                      </Text>
                )}

                {!error && (

                  <View>
                    <Text style={[styles.largeText, styles.textStyle]}>{location}</Text>
                    <Text style={[styles.smallText, styles.textStyle]}>{weather}</Text>
                    <Text style={[styles.largeText, styles.textStyle]}>{getFahreinheitFromC(temperature)}°F</Text>
                  </View>
                )}


                {/* <SearchInput placeholder="Search any city" onSubmit={this.handleUpdateLocation}/> */}
                {
                  <View >
                    <GoogleSearchInput onSubmit={this.handleUpdateLocation} onFocus={this.handleSearchFocus} />
                  </View>
                }

                {consolidatedWeather != null && !placeSearch &&
                  <View style={{ height: deviceHeight }}>

                    <GridView
                      style={styles.gridView}
                      itemDimension={deviceWidth}
                      items={consolidatedWeather}
                      //itemDimension={deviceWidth/2}
                      itemDimension={130}
                      height="100%"
                      renderItem={item => (
                        <View style={styles.itemContainer}>
                          <DailyWeather
                            day='Today'
                            image={item['weather_state_abbr']}
                            minTemp={getFahreinheitFromC(item['min_temp'])}
                            maxTemp={getFahreinheitFromC(item['max_temp'])} />

                        </View>
                      )}
                    />
                  </View>
                }
              </View>

            )}
          </View>
          </ScrollView>
        </ImageBackground>
      </KeyboardAvoidingView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#34495E',


  },
  detailsContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 20,
    paddingVertical: 40

  },
  itemContainer: {
    justifyContent: 'flex-start',
    borderRadius: 20,
    height: 160,
  },
  gridView: {
    zIndex: -1,
    ...Platform.select({
      ios: {
        bottom: deviceHeight / 2 - 400,
      },
      android: {
        bottom: deviceHeight / 2 - 500,
      }
    }),

    height: deviceHeight / 2,
    opacity: .65,

  },
  imageContainer: {
    flex: 1
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  },
  textStyle: {
    textAlign: 'center',
    color: '#ecf0f1',
    ...Platform.select({
      ios: {
        fontFamily: 'AvenirNext-Regular',
      },
      android: {
        fontFamily: 'Roboto',
      }
    })
  },
  largeText: {
    ...Platform.select({
      ios: {
        fontSize: 30,
      },
      android: {
        fontSize: 28,
      }
    })
  },
  smallText: {
    fontSize: 18
  },


});
