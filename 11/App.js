import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground,
  ActivityIndicator,
  StatusBar,
  Dimensions,
  Easing,
  Animated

} from 'react-native';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,

} from 'react-native-indicators';
import GridView from 'react-native-super-grid';
import DailyWeather from './components/DailyWeather';

import { fetchLocationId, fetchWeather, fetchLocationIdByLatLng, fetchPhotosFromUSplash, fetchCity } from '../utils/api';
import getImageForWeather from '../utils/getImageForWeather';
import { getCelciusFromF, getFahreinheitFromC } from '../utils/tempConversions';

import SearchInput from './components/SearchInput';
import GoogleSearchInput from './components/GoogleSearchInput';
import Grid from 'react-native-grid-component';
import moment from 'moment';
import { ScrollView } from 'react-native-gesture-handler';
import { Constants, Location, Permissions } from 'expo';


const windowSize = require('Dimensions').get('window')
const deviceWidth = windowSize.width;
const deviceHeight = windowSize.height;


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: 'Mumbai',
      loading: true,
      error: false,
      temperature: 0,
      weather: '',
      //photo: 'https://hdqwalls.com/download/san-francisco-bridge-aerial-view-5k-bw-1440x2960.jpg',
      photo: 'https://i0.wp.com/wallpaperswide.com/download/cloudy_weather_mountains_landscape-wallpaper-640x960.jpg?resize=640%2C960',
      consolidatedWeather: [],
      placeSearch: false
    }

   // this._animated = new Animated.Value(0);
  }



  componentWillMount() {
    

    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        error: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
        loading: false,
        location: 'San Francisco'
      });
    } else {
      this._getLocationAsync().then(location => {
        //console.log(location);
        this.setState({ location: location.formatted_address });
        this.handleUpdateLocation(this.state.location, location.geometry.location.lat, location.geometry.location.lng);
      });
 }
    // this.handleUpdateLocation(this.state.location);


  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});

    let lat = location.coords.latitude;
    let lng = location.coords.longitude;
    let result = await fetchCity(lat, lng)
    //console.log("Result " , result);
    let firstAddr = await result.results[0];
    return firstAddr;


  };

  handleSearchFocus = async (searchstatus) => {
    //this.setState({placeSearch:searchstatus});
    //console.log("Calling Seach Status");
  }

  handleUpdateLocation = async (city, lat, lng, photo = '') => {

    if (!city) return;


    this.setState({ loading: true }, async () => {
      try {
        const locationId = await ((!lat) ? fetchLocationId(city) : fetchLocationIdByLatLng(lat, lng));
        console.log(locationId);
        const { weather, temperature, consolidatedWeather } = await fetchWeather(locationId);


        await fetchPhotosFromUSplash(city)
          .then(res => res.json())
          .then(data => {
            var results = data['results'];
            var photoRow = results[Math.floor(Math.random() * results.length)];
            var photo = photoRow['urls']['raw'];
            this.setState({ photo: photo });
          })
          .catch(err => {
            return err;
          });


        const location = city;
        this.setState({
          error: false,
          location,
          weather,
          temperature,
          consolidatedWeather,
          placeSearch: false

        });
      }
      catch (e) {
        console.log(e);
        this.setState({
          loading: false,
          error: true,
          location: '',
          consolidatedWeather: [],
        });
      }
    });
  }

  getWeekDay = (date) => {
    //console.log(date);
    var today = moment(moment.now()).format('YYYY-MM-DD');
    var start = moment(today, 'YYYY-MM-DD');
    var end = moment(date, 'YYYY-MM-DD');


    var diff = end.diff(start, "days");
    //console.log(diff);
    var result = "Today";
    switch (diff) {
      case -1:
        result = 'Yesterday';
        break;
      case 0:
        result = 'Today';
        break;
      case 1:
        result = 'Tomorrow';
        break;
      default:
        result = moment(date).format("MMM DD");
    }

    //console.log(result);
    return result;
  }


  render() {
    const { loading, error, location, weather, temperature, photo, consolidatedWeather, placeSearch } = this.state;
    

    return (
      // <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>

      <KeyboardAvoidingView style={styles.container} behavior="padding">
        <StatusBar barStyle="light-content" hidden={true} />

        <ImageBackground          
          source={{ uri: photo }}
          onLoad={() => { this.setState({ loading: false }) }}
          onError={() => { this.setState({ photo: getImageForWeather(weather) }) }}
          style={styles.imageContainer}
          imageStyle={styles.image}>
          {/* <ScrollView keyboardShouldPersistTaps="always">  */}
            <View style={styles.detailsContainer}>
              <View>
                {/* <ActivityIndicator animating={loading} color="white" size="large" style={{ top: (deviceHeight / 2)-40, right: deviceWidth / 2-40, position: 'absolute' }}></ActivityIndicator> */}
                {loading &&
                  <BarIndicator animating={loading} size={60} count={6} color='white' style={{ top: (deviceHeight / 2) - 40, right: deviceWidth / 2 - 90, position: 'absolute' }} />
                }
              </View>
              {!loading && (
                <View>
                  {error && (
                    <View >
                      <Text style={[styles.largeText, styles.textStyle]}></Text>
                      <Text style={[styles.largeText, styles.textStyle]}></Text>
                      <Text style={[styles.largeText, styles.textStyle]}></Text>
                      <Text style={[styles.largeText, styles.textStyle]}></Text>
                      <Text style={[styles.smallText, styles.textStyle]}>
                        {error}
                      </Text>
                    </View>
                  )}

                  {!error && !loading && (

                    <View>
                      <Text style={[styles.largeText, styles.textStyle]}></Text>
                      <Text style={[styles.largeText, styles.textStyle]}></Text>
                      <Text style={[styles.largeText, styles.textStyle]}>{location}</Text>
                      <Text style={[styles.smallText, styles.textStyle]}>{weather}</Text>
                      <Text style={[styles.largeText, styles.textStyle]}>{getFahreinheitFromC(temperature)}°F</Text>
                    </View>
                  )}

                  {!loading && (
                    <View >
                      <GoogleSearchInput onSubmit={this.handleUpdateLocation} onFocus={this.handleSearchFocus} />
                    </View>
                  )}

                  {!error && consolidatedWeather != null &&
                   
                    <GridView
                      style={styles.gridView}
                      itemDimension={deviceWidth}
                      items={consolidatedWeather}
                      itemDimension={120}
                      height="100%"
                      renderItem={item => (

                        <View style={styles.itemContainer}>
                          <DailyWeather
                            day={this.getWeekDay(item['applicable_date'])}
                            weather={item['weather_state_name']}
                            image={item['weather_state_abbr']}
                            minTemp={getFahreinheitFromC(item['min_temp'])}
                            maxTemp={getFahreinheitFromC(item['max_temp'])} />

                        </View>
                      )}
                    />
                   
                  }
                </View>

              )}
            </View>
          {/* </ScrollView> */}
        </ImageBackground>

      </KeyboardAvoidingView>

      //</SafeAreaView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#34495E',
    //marginTop:40

  },
  detailsContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 20,

  },
  itemContainer: {
    justifyContent: 'flex-start',
    borderRadius: 20,
    height: 160,
  },
  gridView: {
    zIndex: -1,
    borderRadius: 20,
    height: deviceHeight / 2,
    marginTop: 60,
    opacity: .60,

  },
  imageContainer: {
    flex: 1,

    //paddingVertical:60
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  },
  textStyle: {
    textAlign: 'center',

    color: '#ecf0f1',
    ...Platform.select({
      ios: {
        fontFamily: 'AvenirNext-Regular',
      },
      android: {
        fontFamily: 'Roboto',
      }
    })
  },
  largeText: {
    ...Platform.select({
      ios: {
        fontSize: 28,
      },
      android: {
        fontSize: 28,
      }
    })
  },
  smallText: {
    fontWeight: '600',
    fontSize: 18
  },
});
