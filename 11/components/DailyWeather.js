import React from 'react';
import {View, Text, Image,StyleSheet, Platform} from 'react-native'
 
import { fetchWeatherImageURL } from '../../utils/api';
import getIconForWeather from '../../utils/getIconForWeather';
import getImageForWeather from '../../utils/getImageForWeather';

export default class DailyWeather extends React.Component{
    
    render(){
        //console.log(fetchWeatherImageURL(this.props.image));
        var imageIcon = '../../assets/'+this.props.image+'.png';
        return(
            <View style = {styles.container}>
            <Text style={[styles.largeText, styles.textStyle]}>{this.props.day}</Text>
            <View style={styles.imageContainer}>
            <Image 
                style={styles.image} 
                //source={getIconForWeather(this.props.image)}
                //source={getImageForWeather('Clear')}
                source={{uri:'https://www.metaweather.com/static/img/weather/png/'+this.props.image+'.png'}}
                //source={require('../../assets/lr.png')}
                //source={require('../../assets/lr.png')}
                resizeMode="contain"
                style={styles.image}
                >
                
            </Image>
            </View>
                
                <Text style={[styles.largeText, styles.textStyle]}>{this.props.weather}</Text>
                <View style={styles.minmax}>
                    <Text style={[styles.largeText, styles.textStyle]}>{this.props.minTemp}°F</Text>
                    <Text style={[styles.largeText, styles.textStyle]}>{this.props.maxTemp}°F</Text>
                </View>
                <View style={styles.minmax}>
                    <Text style={[styles.largeText, styles.textStyle]}>Min</Text>
                    <Text style={[styles.largeText, styles.textStyle]}>Max</Text>
                </View> 
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#2c3e50",
        padding:10,
     },
      minmax: {
        flexDirection: 'row',
       
        justifyContent: "space-around",
        
        
      },
      imageContainer: {
        flex: 1
      },
      image: {
        flex: 1,
        
        paddingVertical: 10
      },
      textStyle: {
        textAlign: 'center',
        color: '#ecf0f1',
        ...Platform.select({
          ios: {
            fontFamily: 'AvenirNext-Regular',
          },
          android: {
            fontFamily: 'Roboto',
          }
        })
      },
      largeText: {
        ...Platform.select({
          ios: {
            fontSize: 18,
          },
          android: {
            fontSize: 20,
          }
        })
      },
      smallText: {
        fontSize: 12
      },
})