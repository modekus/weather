import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground,
  ActivityIndicator,
  StatusBar
} from 'react-native';

import { fetchLocationId, fetchWeather } from '../utils/api';
import getImageForWeather from '../utils/getImageForWeather';

import SearchInput from './components/SearchInput';
import GoogleSearchInput from './components/GoogleSearchInput';




export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: '',
      loading: false,
      error: false,
      temperature: 0,
      weather: '',
    }
  }

  componentDidMount() {
    this.handleUpdateLocation('San Francisco');
  }

  handleUpdateLocation = async city => {
    if (!city) return;

    this.setState({ loading: true }, async () => {
      try {
        const locationId = await fetchLocationId(city);

        const { location, weather, temperature } = await fetchWeather(locationId);

        this.setState({
          loading: false,
          error: false,
          location,
          weather,
          temperature
        });
      }
      catch (e) {
        console.log(e);
        this.setState({
          loading: false,
          error: true,
        });
      }
    });
  }


  render() {
    const { loading, error, location, weather, temperature } = this.state;
    return (
      <View style={styles.container} >
        <StatusBar barStyle="light-content" />

        <View style={styles.detailsContainer}>
        <GoogleSearchInput onSubmit={this.handleUpdateLocation} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#34495E'
  },
  detailsContainer: {
    flex: 2,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 20,

  },
  imageContainer: {
    flex: 1
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  },
  textStyle: {
    textAlign: 'center',
    color: '#ecf0f1',
    ...Platform.select({
      ios: {
        fontFamily: 'AvenirNext-Regular',
      },
      android: {
        fontFamily: 'Roboto',
      }
    })
  },
  largeText: {
    fontSize: 44
  },
  smallText: {
    fontSize: 18
  },

});
