import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';

export default class SearchInput extends React.Component {

    constructor(props){
        super(props);
        this.state={
            location: '',
        };
    }

    handleChangeText = (location) => {
       this.setState({location});
    }

    handleSubmit = () => {
        const {onSubmit} = this.props;
        const{location} = this.state;

        if(!location)
            return;

        onSubmit(location);
        this.setState({location: ''});

    }

    render(){
        return(
            <View style = {styles.container}>
                    <TextInput
                        autoCorrect={false}
                        value={this.state.location}
                        placeholder={this.props.placeholder}
                        placeholderTextColor="white"
                        underlineColorAndroid="transparent"
                        style={styles.textInput}
                        clearButtonMode="always"
                        onChangeText={this.handleChangeText}
                        onSubmitEditing={this.handleSubmit}
                    />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor : '#666',
        height: 50,
        width: 300,
        marginTop: 20,
        marginHorizontal: 20,
        paddingHorizontal: 10,
        alignSelf: 'center',
        borderRadius: 5
      },
      textInput:{
          flex: 1,
          color: 'white'
      }
})