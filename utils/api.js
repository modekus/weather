export  const API_KEY='AIzaSyCiZg3s4lUij2o9rZm9NRi4r5EOaTJllhg';

export const fetchLocationId = async city => {
  
  const response = await fetch(
    `https://www.metaweather.com/api/location/search/?query=${city}`,
  );

  const locations = await response.json();
  
  const woeid = locations[0].woeid;
  //console.log(woeid);
  return woeid;
};

export const fetchLocationIdByLatLng = async (lat,lng) => {
 
  const response = await fetch(
    `https://www.metaweather.com/api/location/search/?lattlong=${lat},${lng}`,
  );
  
  const locations = await response.json();
  return locations[0].woeid;
};

export const fetchWeather = async woeid => {
  const response = await fetch(
    `https://www.metaweather.com/api/location/${woeid}/`,
 );
  
 
  const { title, consolidated_weather } = await response.json();
  
  const { weather_state_name, the_temp } = consolidated_weather[0];

  return {
    weather: weather_state_name,
    temperature: the_temp,
    consolidatedWeather: consolidated_weather
  };
};

export const fetchCity =  async (lat,lng) => {
  
  const response = await fetch(
    'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&sensor=true&key='+API_KEY
  )

  return await response.json();


};
 
export const fetchWeatherImageURL =   (abbr) => {
  var img = 'https://www.metaweather.com/static/img/weather/'+abbr+'.svg';
  //console.log(img);
  return img;
}

export const fetchPhotosFromUSplash = async (city) => {
  
  return await fetch(`https://api.unsplash.com/search/photos/?query=${city}+&orientation=portrait&client_id=9d0a3421038bac165fbc466518eb417d618c367b18540d865c63cd482e471376`)
 

}
