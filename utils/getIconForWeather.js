

/* eslint-disable global-require */
const icons = {
    sn: require('../assets/sn.svg'),
    sl: require('../assets/sl.svg'),
    h: require('../assets/h.svg'),
    t: require('../assets/t.svg'),
    hr: require('../assets/hr.svg'),
    lr: require('../assets/lr.svg'),
    s: require('../assets/s.svg'),
    hc: require('../assets/hc.svg'),
    lc: require('../assets/lc.svg'),
    c: require('../assets/c.svg'),
    
  }

  export default weather => icons[weather];