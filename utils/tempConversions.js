export const getCelciusFromF = (f) => {
    return Math.round((f-32)*5/9);
}

export const getFahreinheitFromC = (c) =>{
    return Math.round((c*9/5)+32);
}