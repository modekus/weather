import React from "react";
import { View, Image, StyleSheet, Platform, Dimensions } from "react-native";
import {
  GooglePlacesAutocomplete
} from "react-native-google-places-autocomplete";

export default class GoogleSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: 'San Francisco',
      lng:'',
      lat:''
    };
  }

  handleChangeText = (latlng, location) => {
    //console.log(latlng['lat']);
    this.setState({ location: location, lat: latlng['lat'], lng: latlng['lng']});
    this.handleSubmit()
  };

  handleSubmit = () => {
    //this.setState()
    const { onSubmit } = this.props;
    const { location, lng, lat } = this.state;
    

    if (!location) return;

    onSubmit(location, lat, lng);
    
  };
  
  render() {
    const windowSize = require('Dimensions').get('window')
    const deviceWidth = windowSize.width-20;
    const deviceHeight = windowSize.height;
    return (
      <View style={styles.container}>
            <GooglePlacesAutocomplete
          enablePoweredByContainer={false}
          placeholder="Search"
          minLength={2}
          autoFocus={false}
          fetchDetails={true}
          onPress={(data, details) => this.handleChangeText(details['geometry']['location'], data['description'])}
          query={{
            types: '(regions)',
            key: 'AIzaSyBlXzW_f3mZD6bOVIsP6bsHhvcICbLD2PQ',
            language: 'en'
          }}
          styles={{
            textInputContainer: {
              backgroundColor: 'rgba(0,0,0,0)'
            },
            listView: {
              height: deviceHeight,
              width: deviceWidth,
              position: 'absolute',
              top: 40
            }
          }}
          nearbyPlacesAPI={'GooglePlacesSearch'}
          filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}

        >
      </GooglePlacesAutocomplete>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    //backgroundColor: "#ecf0f1",
    flex: 1, 
    //marginTop: 50,
    height: 50,
    marginHorizontal: 20,
    paddingHorizontal: 10,
    //alignSelf: "center",
    //borderRadius: 5,
    

  },
  textInput: {
    flex: 1,
    color: "#2c3e50",
    fontSize: 16
  }
});
