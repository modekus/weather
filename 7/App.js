import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View , 
  Platform,
  KeyboardAvoidingView,
  TextInput,
  ImageBackground,
  ActivityIndicator,
  StatusBar,
  Dimensions
} from 'react-native';

import { fetchLocationId, fetchWeather, fetchLocationIdByLatLng } from '../utils/api';
import getImageForWeather from '../utils/getImageForWeather';

import SearchInput from './components/SearchInput';
import GoogleSearchInput from './components/GoogleSearchInput';




export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state={
      location:'',
      loading: false,
      error: false,
      temperature : 0,
      weather: '',
    }
  }

  componentDidMount(){
    this.handleUpdateLocation('San Francisco');
  }

  handleUpdateLocation = async (city,lat, lng) => {
    
    if(!city) return;
    
    
    this.setState({loading: true}, async ()=> {
      try{
        const locationId = await ((!lat) ? fetchLocationId(city): fetchLocationIdByLatLng(lat,lng));
    
        const {weather, temperature} = await fetchWeather(locationId);
        


        const location = city;
        
       

        this.setState({
          loading: false,
          error : false,
          location,
          weather,
          temperature
        });
      }
      catch(e) {
        console.log(e);
        this.setState({
          loading: false,
          error: true,
        });
      }
    });
  }


  render() {
    const { loading, error, location, weather, temperature } = this.state;
  

    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">
      <StatusBar barStyle="light-content" />
      <ImageBackground
                source={getImageForWeather(weather)}
                style={styles.imageContainer}
                imageStyle={styles.image}>
                <View style={styles.detailsContainer}>
                    <ActivityIndicator animating={loading} color="white" size="large"></ActivityIndicator>
                    {!loading && (
                      <View>
                        {error && (
                      <Text style={[styles.smallText, styles.textStyle]}>
                        Could not load weather, please try a different city
                      </Text>
                    )}

                    {!error && (
                      <View>
                          <Text style={[styles.largeText, styles.textStyle]}>{location}</Text>
                          <Text style={[styles.smallText, styles.textStyle]}>{weather}</Text>
                          <Text style={[styles.largeText, styles.textStyle]}>{`${Math.round(temperature)}°`}F</Text>
                      </View>
                    )}
                

                    {/* <SearchInput placeholder="Search any city" onSubmit={this.handleUpdateLocation}/> */}
                    {<GoogleSearchInput onSubmit={this.handleUpdateLocation} />}
                    </View>
                    )}
                    
                
                </View>
      
      
      </ImageBackground>

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex : 1,
    backgroundColor : '#34495E'
  },
  detailsContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 20,
  
  },
  imageContainer : {
    flex : 1
  },
  image : {
    flex : 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  },
  textStyle: {
    textAlign : 'center',
    color: '#ecf0f1',
    ...Platform.select({
      ios:{
        fontFamily : 'AvenirNext-Regular',
      },
      android: {
        fontFamily : 'Roboto',
      }
    })
  },
  largeText : {
    fontSize : 44
  },
  smallText : {
    fontSize: 18
  },
  
});
